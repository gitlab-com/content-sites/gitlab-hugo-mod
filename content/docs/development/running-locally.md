---
title: Running the Handbook Locally
description: Instructions for running the Handbook projects locally
---

### Prerequisites

The handbook development environment requires the **extended** version of [Hugo](https://gohugo.io/).
The extended version is required for the [Docsy GitLab theme](https://gitlab.com/gitlab-com/content-sites/docsy-gitlab)
as we use SASS for generating the CSS stylesheets for the site.  This will also require the installation of Node/NPM.

1. Install [asdf](https://github.com/asdf-vm/asdf) and required plugins. The handbook repositories provide a `.tools-version` configuration file for `asdf`.
1. Install the asdf packages: NodeJS (SASS/CSS), Go (theme development), Vale (linting)
1. Install Git, curl, wget for sync scripts.
1. Install the GitLab CLI for linting jobs.

```shell
brew install asdf git curl wget glab

asdf plugin add hugo
asdf plugin add nodejs
asdf plugin add golang
asdf plugin add vale
asdf plugin add markdownlint-cli2
asdf install

npm install
```

### Running Hugo

```shell
# Be sure to sync-data before building to ensure you have the latest content
./scripts/sync-data.sh

# Build the static website, default output into public/
hugo

# Start a local webserver, listening on localhost:1313
hugo server
```

### Troubleshooting

#### Permission denied errors

If you encounter an error after running `hugo server`:

```shell
hugo server

Watching for changes in /user/path/{Documents,Library}

<snip>

Built in 35042 ms
Error: error copying static files: open /path/to/handbook/public/webfonts/fa-solid-900.ttf: permission denied
```

It means that there might be some stale content from previous Hugo runs in `public/`. You can fix this by running `make clean`.
