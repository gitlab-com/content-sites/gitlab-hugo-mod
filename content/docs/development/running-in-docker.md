---
title: Running the Handbook in Docker
description: Instructions for running the Handbook projects in Docker
---

You can use the Hugo container to start a locally running instance of the handbook, and verify how your changes look.

The following command starts the Hugo server, using the [`hugomods/hugo` container image](https://hugomods.com/docs/docker/#image-tags).
The `exts` container image tag is important, as it provides the required SASS/CSS conversion tools.

```sh
docker run --rm -v $(pwd):$(pwd) -w $(pwd) --network host hugomods/hugo:exts hugo server
```

This will start the Hugo server listening on `http://localhost:1313`. If that doesn't work, try `http://127.0.0.1:1313`. It may take a couple of minutes to load the first time.

You can also start a new container, and run the commands with Hugo manually.

```sh
docker run --rm -it -v $(pwd):$(pwd) -w $(pwd) --network host hugomods/hugo:exts sh

## only if you're running into: ERROR Failed to read Git log: fatal: detected dubious ownership in repository
git config --global --add safe.directory '*'

hugo server

ctrl+d # to quit
```

### Running older Hugo versions

Sometimes, newer Hugo versions introduce breaking changes to the `docsy-gitlab` theme, or its dependencies.
As a workaround, use a working version, for example from the `.gitlab-ci.yml` configuration.

The [Hugomods Docker images](https://docker.hugomods.com/docs/tags/) support versioned tags:

```shell
docker run --rm -v $(pwd):$(pwd) -w $(pwd) --network host hugomods/hugo:exts-0.123.7 hugo serve
exts-0.123.7: Pulling from hugomods/hugo
```
