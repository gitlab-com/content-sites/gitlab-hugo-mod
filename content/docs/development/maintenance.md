---
title: Handbook Maintenance
description: Documentation on how to carry out maintenance tasks.
---

On a regular basis (currently monthly), an issue with a list of maintenance tasks is created and
assigned to the relevant DRIs.

The issues, pipeline schedule, and all relevant code reside in [the maintenance tasks project](https://gitlab.com/gitlab-com/content-sites/handbook-tools/maintenance-tasks).

Note: The maintenance project uses the `TRIAGE_BOT` group access token.
See [access tokens](_index.md#access-tokens) for more on general management of tokens.

## Content tasks

Content tasks are assigned to the [content DRI](https://handbook.gitlab.com/handbook/about/maintenance/#team-structure).
They should consider a designated backup from their team if needed.

### Codeowners check

The "Employment GitLab Bot" should create MRs to remove users who are offboarding through the work done in [`employment-automation#270`](https://gitlab.com/gitlab-com/people-group/peopleops-eng/employment-automation/-/issues/270).
However, we want to double check that it's catching everyone.

In each handbook project:

1. Look for the latest `build` job on the `main` (default) branch.
1. See if there are any messages similar to `WARN  Code owner some-username not found... falling back to public GitLab profile.`
1. Check Slack or Workday to see if these users are no longer at GitLab.
1. Create a MR to remove the users who are no longer at GitLab from the `CODEOWNERS` file.
1. If the user is still at GitLab, create a MR to update the [team member entry](https://gitlab.com/gitlab-com/www-gitlab-com/-/tree/master/data/team_members/person) such that their GitLab username reflects their username.
1. Follow the instructions in the latest "Employment GitLab Bot" MR on where to file an issue for the engineer to investigate why the user was not removed.

### Image compressing

This is a [semi-automated task](#semi-automated-tasks).

<details><summary>Manual process</summary>

If you have Ruby installed, you can use the local version of the script: `ruby ./maintenance-tasks/scripts/compress_images_local.rb -u your-username`

Alternatively:

1. Ensure you have each image compression tool installed locally, [`pngquant`](https://pngquant.org/) and [`jpegoptim`](https://github.com/tjko/jpegoptim).
1. Run the commands below in the root folder for the relevant project.

   ```shell
   find . -name '*.png' -size +100k -print0 | xargs -0 -P8 -L1 pngquant --ext .png --force --skip-if-larger

   ## Run the following a second time with the `.jpeg` extension
   find . -name '*.jpg' -size +50k -print0 | xargs -0 -P8 -L1 jpegoptim -t -T5
   ```

1. Create a MR with the compressed images.

</details>

Note: If you're looking into decreasing GIF file sizes, consider [`Gifsicle`](https://www.lcdf.org/gifsicle/), which you can use to drop frames or other methods. You can combine it with the [Lossy GIF encoder](https://kornel.ski/lossygif) for added compression.

### Image use checking

This is a [semi-automated task](#semi-automated-tasks).

<details><summary>Manual process</summary>

1. Download the [unlinked images script](https://gitlab.com/gitlab-org/gitlab-docs/-/blob/main/tasks/unlinked-images.sh) if you do not already have it, ensuring it's the latest version.
1. Run the script in for each handbook repository: `/path/to/unlinked_images.sh /path/to/handbook/repository`.
1. Make a MR to remove all unlinked files. If there are a lot, consider breaking it up into smaller MRs.
1. Ask the person who added or recently edited the images to review the MR.

</details>

### Link checking (external)

To check links, ensure you have [lychee installed](https://lychee.cli.rs/installation/) locally.

1. Download the [lychee configuration file](https://gitlab.com/gitlab-com/content-sites/handbook-tools/maintenance-tasks/-/blob/main/task_files/lychee.toml) if you do not already have it, ensuring it's the latest version.
1. Ensure each handbook repository is up to date. You will need to complete the task for each repository. The rest of the instructions refer to a single repository.
1. In the root folder of the repository, run `lychee -i -E -u "curl/8.4.0" -b "htts://handbook.gitlab.com" -c ~/Downloads/lychee.toml -f markdown --include-fragments content/**/*.md`, replacing the configuration file location to wherever you have downloaded it to.
   - The base `-b` is included to prevent errors, but handbook links are excluded since we use `hugolint` to check relative links.
   - The insecure `-i`, exclude private `-E`, and user agent `-u` flags are optional. They are included in an attempt to cut down on errors.
1. Edit the repository files to fix all links, as much as possible.
   - If a (sub)domain should be ignored, update the configuration file to exclude it. Domains that require a login should be excluded.
   - If the link redirects, replace the link with the new URL.
   - If a page is under maintenance or unavailable at this time, but it is likely to be available again in the future, leave it as is.
   - If the site still exists, but the page doesn't exist, try searching for a new version.
   - If the site no longer exists, remove the link, and optionally, make a note `(link no longer available)`.
   - Some sites (such as LinkedIn) may be rate limited (throws a 429), check these manually or leave as is.
   - If some pages or folders should be ignored, such as the `node_modules` folder used for local development, add them to the exclusion list.
1. Optionally, run lychee again to verify there are no changes you missed.
1. Once you have fixed the links, create an MR.
1. If needed, create an issue with a report on broken links that you could not fix, and tag the maintainers (or post in a relevant Slack channel).

### Link checking (internal)

Each site has [`hugolint`](_index.md#hugo-lint) running as part of the pipeline to check for broken relative links.

For each site:

1. Look for the latest `hugolint` job.
1. Browse or download the artifacts.
1. The `missinglinks.txt` lists the top 20 broken links, while the `linkcheck` file lists all of them.
1. Fix at least the top 20 from the `missinglinks`. The number before the path indicates the number of occurrences.
   - Most of the time, there is a redirect in place, so visiting the "broken" path will take you to the new one.
   Otherwise, search the same handbook, then the other handbook for a similar page.
1. Create a MR with the fixed links.

Once all relative links are fixed in one of the handbooks, the `hugolint` job should no longer be allowed to fail.
For more information, see the [Improve internal link job epic](https://gitlab.com/groups/gitlab-com/content-sites/-/epics/21).

### Linter related tasks

#### Check for configuration updates

Check for changes to the product documentation [markdownlint](https://gitlab.com/gitlab-org/gitlab/-/blob/master/.markdownlint-cli2.yaml) and [vale](https://gitlab.com/gitlab-org/gitlab/-/tree/master/doc/.vale) configuration changes.

Update the handbook projects as needed. Not all rules are the same as the product documentation.

The [markdown guide](../markdown-guide.md) explains a lot of the usage for `markdownlint`.
Vale usage refers to product documentation as the reasons are the same.

#### Check for and fix errors

To run the linters locally, ensure that you have `markdownlint` and `vale` installed.
You can reference the [product documentation local testing information](https://docs.gitlab.com/ee/development/documentation/testing/#install-documentation-linters) on how to install them.
More information how they are used in the handbook can also be found in [the development docs](_index.md#linting-content).

##### Fix trailing spaces

This is a [semi-automated task](#semi-automated-tasks).

<details><summary>Manual process</summary>

If the job isn't working, here's the manual process.

1. Ensure you have the [task markdownlint config](https://gitlab.com/gitlab-com/content-sites/handbook-tools/maintenance-tasks/-/blob/main/task_files/task.markdownlint-cli2.jsonc) file.
1. Run `markdownlint-cli2 --config tasks/task.markdownlint-cli2.jsonc **/*.md --fix`, replacing the file path with where you have downloaded the configuration file.
1. Commit the changes, and create an MR. You can combine the MR with other linter tasks.

</details>

##### Check and fix errors

The linter jobs do not run on `main` due to how slow they can be.
Part of maintenance then is to double check that errors are not somehow being merged.

The instructions are for a single repository. Do the following for each repository:

1. In the root folder, run `markdownlint-cli2 "content/**/*.md"`.
1. In the root folder, run `vale --no-wrap --minAlertLevel error content/**/*.md`.
1. For each report, note the number of errors in the maintenance issue.
1. Fix all errors.
   - Note: For `markdownlint`, you can run it again with the `--fix` flag to automatically fix some errors.
1. Create a MR with fixes.

Once completed, look back at the previous maintenance issues.
If in the last 3 months, we consistently have errors, discuss with the [handbook DRIs](https://handbook.gitlab.com/handbook/about/maintenance/#team-structure) about adding linting to `main`.

If there are no errors in the last 3 months, we can likely safely remove this task from maintenance.

### Redirects removal

1. In each repository, look for the `index.redirects` file.
1. Remove any redirects past the removal date as noted in a comment.
1. Create a MR.

## Backend tasks

Backend tasks are assigned to the [technical DRI](https://handbook.gitlab.com/handbook/about/maintenance/#team-structure).
They should consider a designated backup from the "Code Maintainer" group if needed.

### Dependencies

The dependencies should be reviewed and bumped as appropriate.

#### Pages to check

When upgrading depencies, we should check that everything is build and renders correctly.

To help with this process, below are a list of elements and examples.

Applies to both public and internal handbook:

1. Navigation bar (at the top of every page): check links, and search.
1. Sidebars: left navigation, and right table of contents.
1. Home page: table of content generation.
1. Images and videos render.
   1. Public: [Values page](https://handbook.gitlab.com/handbook/values/), includes SVG for hierarchy.
   1. Internal: [Self-help troubleshooting](https://internal.gitlab.com/handbook/it/end-user-services/self-help-troubleshooting/)
1. Tableau embeds:
   1. Public: [Support KPIs](https://handbook.gitlab.com/handbook/support/performance-indicators/)
   1. Internal: [KPI indicators](https://internal.gitlab.com/handbook/company/performance-indicators/), click through to a page for charts
1. Mermaid charts:
   1. Public: [Mermaid charts](https://handbook.gitlab.com/handbook/tools-and-tips/mermaid/)
   1. Internal: [Identity verification](https://internal.gitlab.com/handbook/engineering/identity-verification/)

Public handbook:

1. Pages generated using layouts and `www-gitlab-com` data files.
   1. [Team page](https://handbook.gitlab.com/handbook/company/team/)
   1. [Product sections, groups, categories page](https://handbook.gitlab.com/handbook/product/categories/)
   1. [Engineering projects](https://handbook.gitlab.com/handbook/engineering/projects/)
1. Pages generated using shortcodes and data file.
   1. [Miused terms](https://handbook.gitlab.com/handbook/communication/top-misused-terms/)
   1. [Handbook word/page count](https://handbook.gitlab.com/handbook/about/#handbookgitlabcomhandbook)

#### Docsy

Whenever the upstream Docsy theme releases a new version, open a new issue and select the [Docsy-upgrade template](https://gitlab.com/gitlab-com/content-sites/docsy-gitlab/-/blob/main/.gitlab/issue_templates/Docsy-upgrade.md). Follow the steps in the issue to upgrade the theme.

#### Golang / Hugo / NodeJS

It's important to keep the language versions up-to-date and in sync across the projects. When upgrading versions be sure to make those changes to [`docsy-gitlab`](https://gitlab.com/gitlab-com/content-sites/docsy-gitlab/-/merge_requests/133), [`handbook`](https://gitlab.com/gitlab-com/content-sites/handbook/-/merge_requests/6883), and [`internal-handbook`](https://gitlab.com/gitlab-com/content-sites/internal-handbook/-/merge_requests/5008).

Versions are defined in both the `.tool-versions` file **and** the `.gitlab-ci.yml` file. Make sure those versions stay in sync or you might see inconsistent behavior between local and CI builds.

Review changelogs for each tool to be aware of any potential breaking changes. Changelogs can be found:

- [Golang](https://go.dev/doc/devel/release)
- [Hugo](https://github.com/gohugoio/hugo/releases)
- [NodeJS](https://github.com/nodejs/node/blob/main/CHANGELOG.md)

Process:

1. When upgrading dependencies, [test the changes first](_index.md#testing-changes).
1. If there are no concerns, next, release a new version of the Docsy theme with the changes.
1. With the new version of the theme available, create new MRs in `handbook` and `internal-handbook` to apply the version changes and upgrade to the newly released versions of the `docsy-gitlab`. Use review apps for both MRs to test the dependency changes in a production-like environment.

#### markdownlint and vale

The linters shouldn't need to be updated often, but may include fixes or new useful features.

- [markdownlint changelog](https://github.com/DavidAnson/markdownlint/blob/main/CHANGELOG.md), refer to [markdownlint project tags](https://github.com/DavidAnson/markdownlint/tags) for dates
- [Vale releases](https://github.com/errata-ai/vale/releases)

Review the release log for each and determine if a version bump is appropriate. If so:

1. Update your local linter to the appropriate version.
1. Test the linters locally to ensure there are no errors.
1. Bump the version in `.tool-versions` and `.gitlab-ci.yml`.
1. Create MR.

#### Hugo mod

The `hugo mod` command should be used for managing dependencies for handbook projects. Below are some usage examples:

##### Adding a dependency

```shell
hugo mod get server.com/project-name@version
```

Example:

```shell
hugo mod get github.com/google/docsy@v0.9.1
```

##### Tidy

Tidy will clean up unused entries in the `go.mod` and `go.sum` files. This should be run during monthly maintenance.

```shell
hugo mod tidy
```

##### Updating a dependency

To update a specific dependency to its latest version run the following command:

```shell
hugo mod get -u github.com/google/docsy
```

To update all dependencies run:

```shell
hugo mod get -u ./...
```

And remember to commit both `go.mod` and `go.sum` files.

## Semi-automated tasks

Numerous maintenance jobs have been made into manual CI jobs that are created with the maintenance issue.
After 3 months, if there are no issues with the CI jobs when triggered manually, consider making them non-manual jobs to be "automated".

For any "semi-automated job":

1. Run the job by going to the [latest scheduled pipeline in the maintenance project](https://gitlab.com/gitlab-com/content-sites/handbook-tools/maintenance-tasks/-/pipelines).
1. Click on the play button for the relevant job.
1. Optionally, open the job log to see what it's doing.
1. Once the job is finish, you should be assigned 2-3 MRs.
1. If you do not get any MRs assigned, check the job log for errors, or if no changes were necessary.

If needed, check the manual process details to see what might be going wrong, or to run the task manually.

## Automated tasks

Some tasks are automated.

### Clean up stopped environments

GitLab does not automatically delete old stopped environments.

As part of the monthly maintenance schedule, [a CI job](https://gitlab.com/gitlab-com/content-sites/handbook-tools/maintenance-tasks/-/blob/main/.gitlab-ci.yml#L38) uses [the API to delete stopped review apps](https://docs.gitlab.com/ee/api/environments.html#delete-multiple-stopped-review-apps).

Refer to the API calls for the settings.

### Count words and pages

The handbook has [an update counts script](https://gitlab.com/gitlab-com/content-sites/handbook/-/blob/main/scripts/update-counts.sh), which counts the number of words and pages.

The [CI schedule](https://gitlab.com/gitlab-com/content-sites/handbook/-/pipeline_schedules) should be owned by a current handbook DRI. When the job runs, the script with create a MR with the relevant changes and assign it to the user who owns the schedule.

### Delete stale branches

GitLab does not have a feature to bulk delete stale or inactive branches.

As part of the maintenance schedule, [a CI job](https://gitlab.com/gitlab-com/content-sites/handbook-tools/maintenance-tasks/-/blob/main/.gitlab-ci.yml#L52) runs a script to delete branches in the main contente sites repositories where:

1. The related MR was closed or merged more than 1 month ago.
1. No related MR exists and there has been no commit in more than 3 months.
