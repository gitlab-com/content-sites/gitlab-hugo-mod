---
title: "Handbook Search"
description: "Information on how search works on the different handbook sites."
---

The search functionality works differently on the two handbook sites.

## Handbook search

The handbook search uses the free tier of [Algolia](https://www.algolia.com/doc/).

Credentials to access Algolia are stored in a 1password vault named `GitLab.com Service - Handbook bot`. Please submit an access request if you need access.

Algolia is configured in the [config file](https://gitlab.com/gitlab-com/content-sites/handbook/-/blob/main/config/_default/config.yaml).

## Internal handbook search

The internal handbook search uses [Lunr](https://lunrjs.com/), which indexes any Markdown or HTML file created by Hugo.

Lunr was chosen because it does not use a third-party indexing service.
Since internal handbook content is internal only, any search service that is not built into the site needs to go through legal and security reviews.

## Alternative methods for search

There are other ways to search the content on the sites.

1. Search engine: You can specify the handbook site as part of your search terms. See [Searching like a pro](https://handbook.gitlab.com/handbook/tools-and-tips/searching/) for more details.
1. GitLab: Search the repository content using [advanced search](https://docs.gitlab.com/user/search/advanced_search/).
