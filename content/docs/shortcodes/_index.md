---
title: Shortcodes
---

[Shortcodes](https://gohugo.io/content-management/shortcodes/) are pre-defined ways to display content in Markdown files.
If something looks somewhat like a shortcode, but starts with "partial", then it's a [partial](../partials/_index.md).

The shortcodes are listed alphabetically after the shortcode explanatory sections.

Shortcodes are easy to make, so many have been created without documentation.
If you encounter a shortcode that is not listed, please look for the merge request that introduced it to ask the creator to document it,
or ask in the `#handbook` Slack channel.

<!-- markdownlint-disable MD037 -->
## Using shortcodes

There are two ways to call shortcodes:

{{< cardpane >}}
{{< card code=true  header="**Markdown**" lang="md" >}}
{{%/* shortcodename parameters */%}} with markdown rendering
{{</* shortcodename parameters */>}} without markdown rendering
{{< /card >}}
{{< /cardpane >}}

More information can be found in the [Hugo shortcodes documentation](https://gohugo.io/content-management/shortcodes/#use-shortcodes).

### Limitations

At times, shortcode rendering may conflict with Markdown rendering.
As a result, it is recommended to avoid mixing the two,
such as having an alert in a list.

### Shortcode colors

Theme colors:

<i class="fa-solid fa-square -text-primary"></i> `primary`
<i class="fa-solid fa-square -text-secondary"></i> `secondary`
<i class="fa-solid fa-square -text-success"></i> `success`

<i class="fa-solid fa-square -text-info"></i> `info`
<i class="fa-solid fa-square -text-danger"></i> `danger`
<i class="fa-solid fa-square -text-warning"></i> `warning`

<i class="fa-solid fa-square -text-light"></i> `light`
<i class="fa-solid fa-square -text-dark"></i> `dark`

Named colors:

<i class="fa-solid fa-square -text-blue"></i> `blue`
<i class="fa-solid fa-square -text-indigo"></i> `indigo`
<i class="fa-solid fa-square -text-purple"></i> `purple`

<i class="fa-solid fa-square -text-pink"></i> `pink`
<i class="fa-solid fa-square -text-red"></i> `red`
<i class="fa-solid fa-square -text-orange"></i> `orange`

<i class="fa-solid fa-square -text-yellow"></i> `yellow`
<i class="fa-solid fa-square -text-green"></i> `green`
<i class="fa-solid fa-square -text-teal"></i> `teal`

<i class="fa-solid fa-square -text-cyan"></i> `cyan`
<i class="fa-solid fa-square -text-gray"></i> `gray`
<i class="fa-solid fa-square -text-black"></i> `black`

More named colors are available in [Bootstrap Documentation](https://getbootstrap.com/docs/4.6/getting-started/theming/#color).

## Hugo and Docsy shortcodes

The Hugo framework has a few shortcodes included, with a list on the [Hugo shortcodes documentation](https://gohugo.io/content-management/shortcodes/#use-shortcodes). These include:

1. Figure (instead of using HTML)
1. Highlight
1. YouTube and Vimeo embed

The base Docsy theme also has a number of shortcodes, which you can find in [the Docsy shortcodes documentation](https://www.docsy.dev/docs/adding-content/shortcodes/).

Available shortcodes include:

1. Card pane
1. Tabbed panes
1. `iframe`
1. Include content from another file (see also [Includes](#includes))

## Alerts

{{< note >}}
This shortcode is part of the Docsy base theme, not a custom shortcode.
{{< /note >}}

The `color` can be specified, commonly with `warning` (orange), or `danger` (red).
See the [theme colors](#shortcode-colors) for available options.

{{< cardpane >}}
{{< card code=true  header="**Markdown**" lang="md" >}}This is a regular paragraph.

{{%/* alert title="Note" color="primary" */%}}
This is an out of context note that you want to draw a users attention to.
{{%/* /alert */%}}
{{< /card >}}
{{< card header="**Output**" >}}
<p>This is a regular paragraph.</p>
<div class="alert alert-primary" role="alert">
<h4 class="alert-heading">Note</h4>
<p>This is an out of context note that you want to draw a users attention to.</p>
</div>
{{< /card >}}
{{< /cardpane >}}

For a banner at the top of the page, use `pageinfo` instead.

## Comments

These comments are turned into markdown comments.
They are only viewable when viewing the pages source code and are removed before the page is rendered.

{{< cardpane >}}
{{< card code=true  header="**Markdown**" lang="mkd" >}}This is a paragraph.
{{</* comment */>}}
This is a comment which is
completely ignored.
{{</* /comment */>}}
Next paragraph is here.
{{< /card >}}
{{< card header="**Output**" >}}
This is a paragraph.
{{< comment >}}
This is a comment which is
completely ignored.
{{< /comment >}}
Next paragraph is here.
{{< /card >}}
{{< card code=true  header="**Output HTML**" lang="html">}}<p>This is a paragraph.</p>
<p>Next paragraph is here.</p>
{{< /card >}}
{{< /cardpane >}}

## Group pages by category

This custom shortcode can be used on any page to generate a list of other pages in the same page bundle
grouped by category, then optionally subcategory.

For any page within the bundle that does not have either attribute, it uses a default:

1. category: Uncategorized
1. subcategory: General

For information on usage, please look at the [frontmatter section](../frontmatter.md#categorization).

## Includes

We have a custom shortcode that allows you to include content from another file.
Typically, you should use this if you have content that needs to be repeated on more than one page.

While a [Docsy `readfile` shortcode](https://www.docsy.dev/docs/adding-content/shortcodes/#include-external-files) also exists,
the custom include shortcode is preferred.

### Include in the same repository

1. Create the repeated content in its own file `assets/includes/<name-of-snippet>.md`.
2. Insert a copy of the content using code snippet below in the appropriate place on a page.

```text
 {{%/* include "includes/<name-of-snippet>.md" */%}}
 ```

### Include your GitLab user profile readme

Populate a handbook page with your GitLab user profile readme.

1. Create [your GitLab profile readme](https://docs.gitlab.com/ee/user/profile/#add-details-to-your-profile-with-a-readme).
2. Create a handbook page with appropriate [frontmatter](/docs/frontmatter/).
3. For the content of the page, use this code snippet:

   ```go
   {{%/* include-readme username="your-gitlab-username" */%}}
   ```

### Include from another repository

1. Create the file in the appropriate repository, such as the public handbook for use in the internal handbook.
2. Insert a copy of the content using code snippet below with the full URL to the markdown file.

   ```go
   {{%/* include-remote "<url>" */%}}
   ```

### Trimming unneeded content

If you need to trim some lines from the top of the included file when it's displayed in the handbook, specify `trim=x` where `x` is a number.
`trim` removes the top `x + 1` lines from the top of the file. For example, it's useful to remove an unneeded title because we display the value of `Title` instead.

By default, trim is 0, and will not trim any lines.

## Labels

| Attribute          | Description                                                 |
| ------------------ | ----------------------------------------------------------- |
| `name` (required)  | Name for the label. Scoped labels are automatically handled |
| `color` (optional) | Hex color code for the label, defaults to `#428BCA`         |
| `light` (optional) | Only required for light colors to make the label text dark  |

### Unscoped labels

```md
{{</* label name="Category:Container Registry" */>}}
{{</* label name="default-priority" color="#FFECDB" light="true" */>}}
```

{{< label name="Category:Container Registry" >}}
{{< label name="default-priority" color="#FFECDB" light="true" >}}

### Scoped labels

```md
{{</* label name="priority::1" color="#cc0000" */>}}
{{</* label name="Department::Product Security Engineering" color="#69d100" light="true" */>}}
```

{{< label name="priority::1" color="#cc0000" >}}
{{< label name="Department::Product Security Engineering" color="#69d100" light="true" >}}

## Product Categories

There are two different product category shortcodes

`categories-index` gives a list of product categories with a link to the corresponding teams in on the product categories page.

`categories-lookup` provides the same list, but also includes manager names and slack channels as an easy lookup.

## Notes

The classic style notes are smaller and can be used to mention something out of context.

{{< cardpane >}}
{{< card code=true  header="**Markdown**" lang="md" >}}This is a regular paragraph.

{{</* note */>}}
a note is something that needs to be mentioned but is apart from the context.
{{</* /note */>}}
{{< /card >}}
{{< card header="**Output**" >}}
This is a regular paragraph.
{{< note >}}
A note is something that needs to be mentioned but is apart from the context.
{{< /note >}}
{{< /card >}}
{{< /cardpane >}}

## Panels

To add notes and warning blocks into colorful boxes we have provided a shortcode for a panel.
This is similar to the Docsy card shortcode but takes up the whole width of the content area
and can make use of custom header, body, and footer colors.

Use panels when your description contains more than one paragraph, or a
long paragraph. For single and short paragraphs, use alert boxes instead.

Copy paste the following code according to what you want to present to the user
and replace only the description.

{{< cardpane >}}
{{< card code=true  header="**Markdown**" lang="mkd" >}}{{</* panel header="**Note**" header-bg="blue" */>}}
NOTE DESCRIPTION
{{</* /panel */>}}
{{< /card >}}
{{% card header="**Output**" %}}
<div class="card mb-4">
    <div class="card-header -bg-blue">
        <strong>Note</strong>
    </div>
  <div class="card-body">
      <p class="card-text">
          NOTE DESCRIPTION
      </p>
  </div>
</div>
{{% /card %}}
{{< /cardpane >}}

The available colors are listed in the [shortcodes colors section](#shortcode-colors).

## Performance Indicator Embeds

**Examples**

### KPI Summary

```md
{{</* performance-indicators/summary org="example" is_key=true /*/>}}
```

{{< performance-indicators/summary org="pi_example" is_key=true >}}

### Regular PI Summary

```md
{{</* performance-indicators/summary org="example" is_key=false /*/>}}
```

{{< performance-indicators/summary org="pi_example" is_key=false >}}

### Key Performance Indicators

```md
{{</* performance-indicators/list org="example" is_key=true /*/>}}
```

{{< performance-indicators/list org="pi_example" is_key=true >}}


### Regular Performance Indicators

```md
{{</* performance-indicators/list org="example" is_key=false /*/>}}
```

{{< performance-indicators/list org="pi_example" is_key=false >}}

## Tableau Embeds

### `tableau`

**Note:** Any other named parameters are added as additional attributes on the `<tableau-viz>` element.

- `src`: URL of visualization to embed
- `height`: default: `400px`
- `toolbar`: `visible | hidden`, default: `hidden`
- `hide-tabs`: `true | false`, default: `false`

**Examples**

```md
{{</* tableau "https://us-west-2b.online.tableau.com/t/gitlabpublic/views/OKR4_7EngKPITest/PastDueSecurityIssues" /*/>}}
```

{{< tableau "https://us-west-2b.online.tableau.com/t/gitlabpublic/views/OKR4_7EngKPITest/PastDueSecurityIssues" />}}

```md
{{</* tableau height="600px" toolbar="visible" src="https://us-west-2b.online.tableau.com/t/gitlabpublic/views/OKR4_7EngKPITest/PastDueSecurityIssues" */>}}
  {{</* tableau/filters "Subtype Label"="bug::vulnerability" /*/>}}
  {{</* tableau/params "Severity Select"="S2" /*/>}}
{{</* /tableau */>}}
```

<!-- markdownlint-disable-next-line MD013 -->
{{< tableau height="600px" toolbar="visible" src="https://us-west-2b.online.tableau.com/t/gitlabpublic/views/OKR4_7EngKPITest/PastDueSecurityIssues" >}}
  {{< tableau/filters "Subtype Label"="bug::vulnerability" >}}
  {{< tableau/params "Severity Select"="S2" >}}
{{< /tableau >}}

```md
{{</* tableau src="https://10az.online.tableau.com/t/gitlab/views/OKR4_7EngKPITest/PastDueInfraDevIssues" */>}}
  {{</* tableau/filters "Subtype Label"="bug::vulnerability" /*/>}}
{{</* /tableau */>}}
```

<!-- markdownlint-disable-next-line MD013 -->
{{< tableau src="https://10az.online.tableau.com/t/gitlab/views/OKR4_7EngKPITest/PastDueInfraDevIssues" >}}
  {{< tableau/filters "Subtype Label"="bug::vulnerability" >}}
{{< /tableau >}}


### `tableau/filters`

Renders a `viz-filter` element for each of the key/value pairs passed as parameters.

```md
{{</* tableau/filters filter1="value1" filter2="value2" */>}}
```

```html
<viz-filter field="filter1" value="value1"></viz-filter>
<viz-filter field="filter2" value="value2"></viz-filter>
```

### `tableau/params`

Renders a `viz-parameter` element for each of the key/value pairs passed as parameters.

```md
{{</* tableau/params param1="value1" param2="value2" */>}}
```

```html
<viz-parameter name="param1" value="value1"></viz-parameter>
<viz-parameter name="param2" value="value2"></viz-parameter>
```
<!-- markdownlint-enable MD037 -->

## Team Embeds

{{% note %}}
These shortcodes currently exist in the [handbook](https://gitlab.com/gitlab-com/content-sites/handbook) project only.
{{% /note %}}

### `team-by-manager-role`

Renders a table containing a team and their manager. These are parsed and built from `team.yml` entries. This can be used in one of two ways:

- Single Positional Argument
- Named Arguments

The output renders a table element that renders the `member/with-team-link` partial (from the `docsy-gitlab` project):

```html
<table>
    <thead>
        <th>Name</th>
        <th>Role</th>
    </thead>
    <tbody>
        <tr>
          <td><member/with-team-link partial for manager></td>
          <td><manager's role></td>
        </tr>
          <tr>
              <td><member/with-team-link partial for team member></td>
              <td><team member's role></td>
          </tr>
    </tbody>
</table>
```

If no manager is found, this shortcode will return an empty table.

#### Single Positional Argument

```md
{{</* team-by-manager-role "Engineering Manager(.*)Plan:Product Planning" */>}}
```

This would search `team.yml` entries for a matching entry of the regex, to return a single manager entry. All other entries that have a `reports_to` field pointing to the manager are then rendered.

#### Named Arguments

```md
{{</* team-by-manager-role role="Engineering Manager(.*)Create:Source Code" team="[,&] (Create:Source Code)" */>}}
```

This searches for a `team.yml` entry matching the regex given in `role` for the manager. All entries with a role matching `team` are then returned as team members.

### `team-by-manager-slug`

Renders a table containing a team and their manager. These are parsed and built from `team.yml` entries. This can be used in one of two ways:

- Single Positional Argument
- Named Arguments

The output renders a table element that renders the `member/with-team-link` partial (from the `docsy-gitlab` project):

```html
<table>
    <thead>
        <th>Name</th>
        <th>Role</th>
    </thead>
    <tbody>
        <tr>
          <td><member/with-team-link partial for manager></td>
          <td><manager's role></td>
        </tr>
          <tr>
              <td><member/with-team-link partial for team member></td>
              <td><team member's role></td>
          </tr>
    </tbody>
</table>
```

#### Single Positional Argument

```md
{{</* team-by-manager-slug "manager.name" */>}}
```

This would search `team.yml` entries for a matching slug of `manager.name`, to return a single manager entry. All other entries that have a `reports_to` field pointing to the manager are then rendered.

#### Named Arguments

```md
{{</* team-by-manager-slug manager="nmccorrison" team="Engineer(.*)Govern:Threat Insights" */>}}
```

This would search `team.yml` entries for a matching slug of `manager.name`, to return a single manager entry. All entries with a role matching the regex in `team` *and* a `reports_to` matching that manager slug are then returned as team members.

### `team-by-departments`

Renders a table containing a team and optionally, their manager. These are parsed and built from `team.yml` entries. This can be used in one of two ways:

- Single Positional Argument
- Named Arguments

The output renders a table element that renders the `member/with-team-link` partial (from the `docsy-gitlab` project):

```html
<table>
    <thead>
        <th>Name</th>
        <th>Role</th>
    </thead>
    <tbody>
        <tr>
          <td><member/with-team-link partial for manager></td>
          <td><manager's role></td>
        </tr>
          <tr>
              <td><member/with-team-link partial for team member></td>
              <td><team member's role></td>
          </tr>
    </tbody>
</table>
```

#### Single Positional Argument

```md
{{</* team-by-departments "department-name" */>}}
```

This would search `team.yml` entries for team members where one of the entries in the `departments` array is `department-name`.

#### Named Arguments

##### Variation 1

```md
{{</* team-by-departments department="department-name" */>}}
```

This is identical to using a single positional argument.

##### Variation 2

```md
{{</* team-by-departments departments="department-name-1,department-name-2" */>}}
```

This will search `team.yml` for a list of team members whose `departments` array has at least one element in the given `departments` parameter.

##### Variation 3

```md
{{</* team-by-departments departments="department-name-1,department-name-2" manager-role="manager, department-name" */>}}
```

This will search `team.yml` for a list of team members whose `departments` array has at least one element in the given
`departments` parameter.

In addition, the first team member whose `role` entry matches the regular expression given in the `manager-role`
parameter. The manager's partial will be displayed at the top of the table and team members will follow.
