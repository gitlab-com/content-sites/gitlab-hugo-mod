---
title: Partials
---

[Hugo partials](https://gohugo.io/templates/partial/) are bits of reusable content.
They're primarily used for parts of theme files, other HTML (such as layout) files, and within shortcodes.

Partials may exist in the theme for use on all content sites, or in a specific repository, such as the handbook.
Many partials are duplicated, sometimes with minor edits, in the handbook projects.

{{% alert color="info" %}}
When deciding which to create, if the reusable content is meant be used only in Markdown files,
create a [shortcode](../shortcodes/_index.md) instead.
{{% /alert %}}

Partials are easy to make, so many have been created without documentation.
If you encounter a partial that is not listed, please look for the merge request that introduced it to ask the creator to document it,
or ask in the `#handbook` Slack channel.

Partials as listed in alphabetical order after the theme section.

## Theme and layout partials

A lot of the partials are used for the theme, or within the basic layout files.
These partials should not be used on their own.

Including:

1. `about-gitlab.html` used in `layouts/job-families`.
1. `controlled-document.html` shows the relevant banner on any page with [controlled document set to true in the frontmatter](../frontmatter.md#controlled-documents).
1. `footer.html` on all pages.
1. `page-meta-lastmod.html` and `page-meta-links.html` for last commit, and view/edit page links.
1. `section-index.html` to show the list of subpages if any.
1. `gitlab/codeowner(s).html` for showing the "Maintained by" list in the right sidebar.
1. `hooks/` folder for any hooks required in the theme.

## Math

The `math.html` partial is used for the [math embed](../markdown-guide.md#math).

## Member

These partials are used frequently in shortcodes, other partials, and in Markdown files.

Typically, there is a list of team member slugs, and the partial is used to display a list of team members using one of the partials.

For example, [the handbook `group-table.html`](https://gitlab.com/gitlab-com/content-sites/handbook/-/blob/main/layouts/partials/group-table.html) partial is used to list the members of a product group, which uses the `member-` partials.

## News related

The internal handbook used to have "news" content, with the latest items featured on the home page.

As the internal communications team no longer uses it, the content was removed and the home page edited to no longer use that partial to display the news items.
Howevers, the partials have been kept for the time being.

Including:

1. `company-fyi.html`
1. `featured-news.html`

## Performance indicators

These partials are used for the [performance indicator shortcode](../shortcodes/_index.md#performance-indicator-embeds).

## `tableau-with-filters`

If you're looking for the Tableau embed, see the [Tableau shortcode](../shortcodes/_index.md#tableau).

This partial is the primary way for embedding Tableau charts in the handbooks.
It should be used in all shortcodes that need to embed a tableau chart.
It is set up to a named parameters or a data file as the source.
In each shortcode this partial is called a flag must be set, `hastableau=true`,
so that the correct JavaScript is applied to the page at the time of construction.

- parameters
  - `url`: (required)
    - The shareable URL of the view to be embedded
  - `tabs`: (optional defaults to false)
    - A flag to indicate if other views should be shown as tabs, `true | false`
  - `toolbar`: (optional defaults to hidden)
    - A value to indicate the location, including hidden, of the tool bar as part of the embed. `hidden | top | bottom`
  - `height`: (optional defaults to 300px)
    - The fixed height of the embedded view in pixels.
  - `filters`: (optional)
    - A list of filters and values (key value pairs) to filter the view by.
    When passing the chart data directly filter fields and their values should be separated by an equal symbol, `=`,
    and subsequent fields should be separated with a double pipe symbol, `||`,
    and if multiple values are used in a single filter the values should be separated by a comma symbol `,`.
  - `parameters`: (optional)
    - A list of parameters and values (key value pairs) to set in the view.
    When passing the chart data directly parameter names and their values should be separated by an equal symbol, `=`,
    and subsequent parameters should be separated with a double pipe symbol, `||`.

When referencing the chart data directly

```go
{{ .Page.Store.Set "hastableau" true -}}

// shortcode content

{{ partial "tableau-with-filters" url="url" tabs="true" toolbar="top" height="200px"   filters="field=value||field=value,value,value" parameters="name=value||name=value" }}
```

When using a data file for the chart data.

```go
{{ .Page.Store.Set "hastableau" true -}}

// shortcode content that gets chart information from a data file

{{- with .tableau_data }}
{{- range .charts }}
{{- partial "tableau-with-filters" . }}
{{- end }}
{{- end }}
```
