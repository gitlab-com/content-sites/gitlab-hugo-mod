---
title: "Handbook frontmatter"
description: "Information on what frontmatter entries are available and how to use them."
controlled_document: true
---

This page covers what frontmatter information is available on handbook pages, and their usage.

## Format and title

At the top of the file, start with three hyphens, frontmatter entries, closing with
three more hyphens:

```plain
---
title: "Page title"
other_attribute: "see other sections for what's valid"
---
```

Every page should have a title at minimum.

Titles should use [sentence case](https://www.thoughtco.com/sentence-case-titles-1691944).

## Description

A description is very useful to provide a summary and will be displayed in the list of leaf pages.

Whether a list of leaf pages is displayed depends on the layout.
By default, the list is shown at the bottom of the page.

```plain
description: "Summary of page content."
```

## Aliases

{{% alert title="Note" color="warning" %}}
Please do not use an alias unless the redirect needs to be kept permanently.
Use a [redirect](/docs/development/#redirects) instead.
{{% /alert %}}

The aliases provide a list of other paths the current page should be for.
Essentially, it's providing a redirect paths.

If a user visits one of the listed aliases, it will redirect to the current page.

```plain
aliases:
- /handbook/page/path/
- /handbook/old/page/location/
```

## Categorization

When used in combination with the [group pages by category shortcode](shortcodes/_index.md#group-pages-by-category), a grouped list of pages can be generated.

```plain
---
category: "Some category name"
subcategory: "Some subcategory"
---
```

At the moment, the shortcode only accepts one entry. It will not work with multiple categories for a single page.

## Controlled Documents

[Controlled documents](https://handbook.gitlab.com/handbook/security/controlled-document-procedure/) require the handbook frontmatter attribute `controlled_document` set to `true`. This attribute also renders the warning header, linking to [controlled documents review section](https://handbook.gitlab.com/handbook/security/controlled-document-procedure/#review). Example:

```plain
---
title: "Controlled Document Procedure"
description: "GitLab deploys control activities through policies and standards that establish what is expected and procedures that put policies and standards into action."
controlled_document: true
---

```

Some pages might not need the warning banner which can optionally be disabled using the `controlled_document_banner_disabled` attribute.

```plain
---
title: "Public document without banner"
description: "..."
controlled_document: true
controlled_document_banner_disabled: true
---

```

In addition to the frontmatter for individual pages, controlled documents and their codeowner(s) must be listed in the "Controlled Documents" section of the repository's `CODEOWNERS` file. Please also follow any instructions at the top of the `CODEOWNERS` file.

## Page Types (Layouts)

**Note:** To keep the handbook consistent, we discourage overriding the page `type` (aka layout).

You may notice that some pages have `layout` in the frontmatter, such as:

```plain
---
title: "Some page"
layout: default
---
```

The `layout` keyword was used in the old `www-gitlab-com` project and is no longer relevant. Please remove it if you see it.

We now use the Docsy theme, which includes some [provided layouts](https://www.docsy.dev/docs/adding-content/content/#content-sections-and-templates). By default, all handbook pages inherit the `docs` type specified [in `content/handbook/_index.md`](https://gitlab.com/gitlab-com/content-sites/handbook/-/blob/main/content/handbook/_index.md?ref_type=heads&plain=1#L8-9).

If you are adding a new top-level section to the site, we encourage you to do the same:

```plain
<!-- content/<new_section>/_index.md -->
---
title: New Section
cascade:
- type: docs
---

Welcome to this new section of the handbook site!
```

If you would like to simplify or remove the list of pages in the section, you can set the `simple_list` or `no_list` attribute to `true` following the [Docsy theme landing page section documentation](https://www.docsy.dev/docs/adding-content/content/#docs-section-landing-pages).

One exception to page type inheritence is the "news" section of the internal-handbook which [inherits the `blog` page type.](https://gitlab.com/gitlab-com/content-sites/internal-handbook/-/blob/main/content/news/_index.md?ref_type=heads&plain=1#L4)

## Math

If the page needs to render [mathematical and other formulae](/docs/markdown-guide/#math) using LaTex then the `math` parameter must be set to `true`

```plain
math: true
```
