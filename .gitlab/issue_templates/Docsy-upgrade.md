### Upgrading upstream Docsy theme

NOTE:
You will need `hugo` or `docker` locally to perform the upgrade steps.

Whenever the upstream [Docsy](https://github.com/google/docsy) theme [releases a new version](https://github.com/google/docsy/releases),
we need to follow these steps to update the Docsy GitLab theme and ensure backwards compatibility.
For example, to upgrade to `docsy:v0.10.0` ([sample MR](https://gitlab.com/gitlab-com/content-sites/docsy-gitlab/-/merge_requests/82)):

1. Create a new branch for the upgrade `deps-upgrade-docsy-v0-10-0`.
1. Update the `go.mod` file to use the new `docsy` version:

   ```diff
   diff --git a/go.mod b/go.mod
   index 452ab3a..b591d1e 100644
   --- a/go.mod
   +++ b/go.mod
   @@ -3,6 +3,6 @@ module gitlab.com/gitlab-com/content-sites/docsy-gitlab
    go 1.19

    require (
   -	github.com/google/docsy v0.9.1 // indirect
   +	github.com/google/docsy v0.10.0 // indirect
    	github.com/google/docsy/dependencies v0.7.2 // indirect
    )
   ```

1. Run the following commands to update the `hugo mod` dependencies.

   ```shell
   # Run the Hugo docker image and shell into it
   docker run -ti -v $PWD:/app -w /app registry.gitlab.com/pages/hugo/hugo_extended:0.124.1 sh
   # update and install Go
   apk update
   apk add go
   # sync and fix dependencies
   hugo mod tidy
   exit
   ```

1. Commit and push your changes. Changes to the `go.mod` and `go.sum` files are expected.
1. Copy the branch name of the `docsy-gitlab` containing the upgraded `docsy` version. For example `deps-upgrade-docsy-v0-10-0`.
1. To update the `handbook` and the `internal-handbook` projects, you need to change the `go.mod` files, pointing to the branch name instead of the version:

   ```diff
   diff --git a/go.mod b/go.mod
   index f23c27e59b..6f3c8d4740 100644
   --- a/go.mod
   +++ b/go.mod
   @@ -6,5 +6,5 @@ require github.com/google/docsy v0.9.1

    require (
    	github.com/google/docsy/dependencies v0.7.2 // indirect
   -	gitlab.com/gitlab-com/content-sites/docsy-gitlab v0.3.29 // indirect
   +	gitlab.com/gitlab-com/content-sites/docsy-gitlab deps-upgrade-docsy // indirect
    )
   ```

1. Update the Hugo dependencies in the `handbook` and `internal-handbook` projects by running the same command as above

   ```shell
   # Run the Hugo docker image and shell into it
   docker run -ti -v $PWD:/app -w /app registry.gitlab.com/pages/hugo/hugo_extended:0.124.1 sh
   # update and install Go
   apk update
   apk add go
   # sync and fix dependencies
   hugo mod tidy
   exit
   ```

1. The previous actions will update the `go.mod` and `go.sum` files. For example:

   ```diff
   diff --git a/go.mod b/go.mod
   index f23c27e59b..21ea3b5f32 100644
   --- a/go.mod
   +++ b/go.mod
   @@ -2,9 +2,9 @@ module gitlab.com/gitlab-com/content-sites/handbook

    go 1.19

   -require github.com/google/docsy v0.9.1
   +require github.com/google/docsy v0.10.0

    require (
    	github.com/google/docsy/dependencies v0.7.2 // indirect
   -	gitlab.com/gitlab-com/content-sites/docsy-gitlab v0.3.29 // indirect
   +	gitlab.com/gitlab-com/content-sites/docsy-gitlab v0.3.30-0.20240603014535-10be2ef40c6c // indirect
    )
   ```

1. Commit your changes and open an MR.
1. Navigate to the MR's `Pipelines` tab and run the `pages` manual job in the `deploy` stage:

   ![Deploy Pages job](img/pipeline-deploy-pages.png)

1. Click on the job and wait for it to finish. Once it's done,
  look for the URL of the deployed site with a similar message to
  `Pages accessible through https://gitlab-com.gitlab.io/content-sites/handbook/mr<NUM>`.
1. Perform some basic manual tests on the site. See the [list of pages to check](https://handbook.gitlab.com/docs/development/maintenance/#pages-to-check).

1. Post a message in [#handbook](https://gitlab.enterprise.slack.com/archives/C81PT2ALD) with the URL of the deployed
Pages review site and ask a few folks to help test the upgrade.
1. If all the tests pass, close the handbook and internal-handbook MRs.
1. Merge the Docsy GitLab MR, and follow the standard release process.
