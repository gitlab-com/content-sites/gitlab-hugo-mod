#!/usr/bin/env ruby

require 'open3'

def exec_cmd(cmd)
  output, stderr, status = Open3.capture3(cmd)

  raise "#{stderr} when executing #{cmd}" unless status.success?

  output
end

def exec_log_cmd(cmd)
  
  puts "Executing #{cmd}..."

  Open3.popen3(cmd) do |stdin, stdout, stderr, thread|
    out_thread = Thread.new do
      until (line = stdout.gets).nil? do
        puts "#{line}"
      end
    end
  
    err_thread = Thread.new do
      until (line = stderr.gets).nil? do
        puts " #{line}"
      end
    end
  
    out_thread.join
    err_thread.join
    exit_status = thread.value

    puts "Command Failed" unless exit_status.success?
  end
end

def get_latest_tag
  exec_cmd("git describe --tags `git rev-list --tags --max-count=1`")
end

def get_sha_for_tag(tag)
  exec_cmd("git rev-list -n 1 #{tag}")
end

def get_commits_since(sha)
  exec_cmd("git log --no-merges --pretty=format:'%s' #{sha.strip}..HEAD")
end

def increment_patch_version(tag)
  parts = tag.split('.')

  parts[-1] = parts[-1].to_i + 1

  parts.join('.')
end

def format_tag_description(commits)
  output = []
  commits.lines.each do |commit|
    output << "- #{commit.strip}"
  end
  output.join("\n")
end

def create_release!(new_version, description)
  exec_log_cmd("glab release create #{new_version} --notes \"#{description.gsub('"', '\"')}\"")
end

def configure_git
  if ENV["CI"]
    exec_log_cmd("git config --global user.email \"$GITLAB_USER_EMAIL\"")
    exec_log_cmd("git config --global user.name \"$GITLAB_USER_NAME\"")
  end
end

def create_mr(project_name, new_version)
  url = "https://oauth2:#{ENV['GITLAB_TOKEN']}@gitlab.com/gitlab-com/content-sites/#{project_name}.git"

  exec_log_cmd("git clone #{url}")
  Dir.chdir(project_name)

  exec_log_cmd("git checkout -b docsy-gitlab-upgrade-#{new_version}")
  exec_log_cmd("go clean -cache -modcache")
  exec_log_cmd("go get gitlab.com/gitlab-com/content-sites/docsy-gitlab@#{new_version}")
  exec_log_cmd("hugo mod tidy")
  exec_log_cmd("git add go.mod go.sum")
  exec_log_cmd("git commit -m \"Upgraded docsy-gitlab to #{new_version}\"")
  exec_log_cmd("glab auth status")
  exec_log_cmd("glab mr create -f -y -a #{ENV['GITLAB_USER_LOGIN']} --remove-source-branch --squash-before-merge --label Handbook::Operations")
end

# get the most recent tag from git
latest_tag = get_latest_tag

# get the sha for the most recent tag
sha_for_tag = get_sha_for_tag(latest_tag)

# get all commits since the last tag was created
commits = get_commits_since(sha_for_tag)

# format the commit messages for the tag description
description = format_tag_description(commits)

# create a new version number for the new tag
new_version = increment_patch_version(latest_tag)

# create release with glab
create_release!(new_version, description)

# Configure local git user
configure_git

projects = ["handbook", "internal-handbook"]

projects.each do |project|
  create_mr(project, new_version)
end